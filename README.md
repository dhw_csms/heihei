# Heihei

**デジタルハリウッド大学 2020学園祭に展示予定のプロジェクト**

**制作: 自作ゲームサークルCsMS(コスモス)**

参考チュートリアル: https://www.udemy.com/course/kart-racing/

ログインはマサキ(a18dc593@dhw.ac.jp)に尋ねてください。

全てのsectionにある機能を実装しました。後は他の必要機能を実装します。(2020-06-23)

必要なツール:
1. Unity 2019.2.17f1 以上
2. Git

プロジェクトUnityバージョン: 2019.2.17f1

オススメツール:
1. Github Desktop(Windows) / Tortoise Git(Windows) / SourceTree(MAC)
2. Visual Studio Community 2017 以降
3. Unity Hub

環境構築:
1. https://gitlab.com/dhw_csms/heihei.git をcloneしてください。(場所はどこでも良い)
2. Unityを立ち上げて、Heiheiのフォルダを選択してください。(Unityバージョンが違う場合は"Continue"ボタンをしてください)
3. "Asset Database Version Upgrade"ポップアップがあればに"Yes"を選択してください。
4. "Scene"フォルダの"Heihei_Demo_Track"を選択して、シーンを確認してください。
5. "Console"に現れたwarning / errorをclearすれば大丈夫です。

作業手順:
1. "master"ブランチをpullしてください。(最新コードを確認するため)
2. "master"をベースとして、ブランチを作成してください。
ブランチの名前は、何でもいいですが、"{自分の名前}-{作業内容の名前}"がオススメです
3. 作業が終わったら、そのブランチにコミットして、コメントで作業内容を入力してください。
4. "以下のフォルダをコミットしないでください:
- "Library"フォルダ中の全てフォルダ / ファイル

(ローカル環境で"Add to ignore list"でいいです)

5. コミットしたらプッシュしてください。
6. LINEでCsMSの団長(マサキ)に声をかけてください。
